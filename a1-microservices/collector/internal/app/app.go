package app

import (
	"fmt"

	"go.uber.org/multierr"
	"go.uber.org/zap"
)

type App struct {
	Logger   *zap.Logger
	detector HumanDetection
	cryminal CryminalCheck
	analysis ImageAnalysis
}

func NewApp(l *zap.Logger) App {
	return App{
		Logger:   l,
		detector: HumanDetection{Logger: l},
		cryminal: CryminalCheck{Logger: l},
		analysis: ImageAnalysis{Logger: l},
	}
}

func (a App) HandleFrame(frame Frame) error {
	num, err := a.detector.DetectHumans(frame)
	if err != nil {
		return err
	}

	a.Logger.Debug(fmt.Sprint("detected ", num, " humans"))

	if num > 0 {
		var processErr error
		// check against a list of known persons of interest
		err = a.cryminal.CheckFaces(frame)
		processErr = multierr.Append(processErr, err)

		// forward to analysis
		err = a.analysis.Analyse(frame)
		processErr = multierr.Append(processErr, err)

		return processErr
	}

	return nil
}
