package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"
)

const (
	cryminalCheckUrl         = "http://anticriminal/frame"
	cryminalCheckContentType = "application/json"
)

type CryminalCheck struct {
	Logger *zap.Logger
}

func (c CryminalCheck) CheckFaces(frame Frame) error {
	c.Logger.Debug("send for cryminal checkup")

	marshalledBody, err := json.Marshal(frame)
	if err != nil {
		return errors.New("failed to marshal body when sending the request: " + err.Error())
	}
	reqBody := bytes.NewBuffer(marshalledBody)

	resp, err := http.Post(cryminalCheckUrl, cryminalCheckContentType, reqBody)
	if err != nil {
		return errors.New("failed to post the frame to the cryminal checkup: " + err.Error())
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = errors.New("failed to post the frame to the cryminal checkup, response status: " + resp.Status)
	}

	return err
}
