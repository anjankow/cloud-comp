package app

const (
	EventExit  = "exit"
	EventEnter = "entry"
)

type Frame struct {
	Timestamp      string `json:"timestamp"`
	Image          string `json:"image"`
	Section        int    `json:"section"`
	Event          string `json:"event"`
	DestinationUrl string `json:"destination"`
	ExtraInfo      string `json:"extra-info"`
}
