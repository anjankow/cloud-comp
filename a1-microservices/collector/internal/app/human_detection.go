package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

const (
	humanDetectionUrl         = "http://human-detection/frame"
	humanDetectionContentType = "application/json"
)

type HumanDetection struct {
	Logger *zap.Logger
}

type humanDetectionRsp struct {
	Timestamp   string `json:"timestamp"`
	Image       string `json:"image"`
	Section     int    `json:"section"`
	Event       string `json:"event"`
	PersonCount int    `json:"person-count"`
	ExtraInfo   string `json:"extra-info"`
}

func (h HumanDetection) DetectHumans(frame Frame) (int, error) {
	h.Logger.Debug("detect humans")

	marshalled, err := json.Marshal(frame)
	if err != nil {
		return 0, errors.New("failed to create human detection request: " + err.Error())
	}
	reqBody := bytes.NewBuffer(marshalled)

	resp, err := http.Post(humanDetectionUrl, humanDetectionContentType, reqBody)
	if err != nil {
		return 0, errors.New("failed to post human detection request: " + err.Error())
	}
	defer resp.Body.Close()

	h.Logger.Debug("human detection response received", zap.Any("code", resp.StatusCode), zap.Error(err))

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, errors.New("failed to read human detection response: " + err.Error())
	}

	var humanDetectionRsp humanDetectionRsp
	err = json.Unmarshal(respBody, &humanDetectionRsp)
	if err != nil {
		return 0, errors.New("failed to unmarshal human detection response: " + err.Error())
	}

	return humanDetectionRsp.PersonCount, nil
}
