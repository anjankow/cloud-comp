package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"
)

const (
	imageAnalysisUrl         = "http://image-analysis/frame"
	imageAnalysisContentType = "application/json"
	imageAnalysisDestination = "http://section/persons"
)

type ImageAnalysis struct {
	Logger *zap.Logger
}

func (a ImageAnalysis) Analyse(frame Frame) error {

	a.Logger.Debug("analyze the image")

	frame.DestinationUrl = imageAnalysisDestination

	postBody, err := json.Marshal(frame)
	if err != nil {
		return errors.New("failed to marshal body when sending the request: " + err.Error())
	}
	requestBody := bytes.NewBuffer(postBody)

	resp, err := http.Post(imageAnalysisUrl, imageAnalysisContentType, requestBody)
	if err != nil {
		return errors.New("failed to post the frame to the analysis: " + err.Error())
	}
	defer resp.Body.Close()

	// the response is redirected to the section service - no need to check the body
	if resp.StatusCode != http.StatusOK {
		err = errors.New("failed to post the frame to the analysis: " + resp.Status)
	}

	return err
}
