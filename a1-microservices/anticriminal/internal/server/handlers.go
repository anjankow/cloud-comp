package server

import (
	"anticriminal/internal/app"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

// adds a new frame to the collector service
func frame(a *app.App, w http.ResponseWriter, r *http.Request) (int, error) {
	method := "POST"
	if r.Method != method {
		return http.StatusMethodNotAllowed, errors.New("incorrect method type: expected: " + method + ", received: " + r.Method)
	}

	contentType := "application/json"
	if r.Header.Get("Content-Type") != contentType {
		return http.StatusUnsupportedMediaType, errors.New("incorrect content-type: expected: " + contentType + ", received: " + r.Header.Get("Content-Type"))
	}

	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return http.StatusInternalServerError, errors.New("can't read the request body: " + err.Error())
	}

	var frame app.Frame
	if err = json.Unmarshal(body, &frame); err != nil {
		return http.StatusBadRequest, errors.New("invalid body: " + err.Error())
	}

	// add to the queue
	a.HandleFrame(frame)

	return http.StatusOK, nil
}

func healthcheck(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("all good here"))
}
