package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

const (
	faceRecognitionUrl         = "http://face-recognition/frame"
	faceRecognitionContentType = "application/json"
)

type faceRecognition struct {
	Logger *zap.Logger
}

type faceRecognitionRsp struct {
	Timestamp    string
	Image        string
	Section      int
	Event        string
	ExtraInfo    string   `json:"extra-info"`
	KnownPersons []Person `json:"known-persons"`
}

type faceRecognitionReq struct {
	Timestamp string
	Image     string
	Section   int
	Event     string
	ExtraInfo string `json:"extra-info"`
}

func (a faceRecognition) Recognize(frame Frame) (persons []Person, err error) {
	a.Logger.Debug("recognize the faces")

	body := faceRecognitionReq{
		Timestamp: frame.Timestamp,
		Image:     frame.Image,
		Section:   frame.Section,
		Event:     frame.Event,
		ExtraInfo: frame.ExtraInfo,
	}

	marshalledBody, err := json.Marshal(body)
	if err != nil {
		return persons, errors.New("failed to marshal body when sending the request: " + err.Error())
	}
	resp, err := http.Post(faceRecognitionUrl, faceRecognitionContentType, bytes.NewBuffer(marshalledBody))

	if err != nil {
		return
	}
	if resp == nil {
		a.Logger.Debug("response is nil")
		return
	}

	a.Logger.Debug("response received", zap.Any("code", resp.StatusCode))
	if resp.StatusCode != http.StatusOK {
		return
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	var recognized faceRecognitionRsp
	if err = json.Unmarshal(respBody, &recognized); err != nil {
		return persons, errors.New("can't unmarshal face recognition response: " + err.Error())
	}

	if len(recognized.KnownPersons) == 0 {
		a.Logger.Debug("no persons recognized", zap.Any("persons", recognized.KnownPersons))
		return
	}

	a.Logger.Debug("recognized", zap.Int("number", len(recognized.KnownPersons)), zap.Any("persons", recognized.KnownPersons))

	return recognized.KnownPersons, nil
}
