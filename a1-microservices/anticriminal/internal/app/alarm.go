package app

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"

	"go.uber.org/zap"
)

const (
	alarmUrl         string = "http://alert/alerts"
	alarmContentType string = "application/json"
)

type alarm struct {
	Logger *zap.Logger
}

type alarmReq struct {
	Timestamp    string
	Section      int
	Exent        string
	KnownPersons []Person `json:"known-persons"`
	Image        string
	ExtraInfo    string `json:"extra-info"`
}

func (a alarm) Create(persons []Person, frame Frame) error {
	a.Logger.Debug("create an alarm", zap.String("person", persons[0].Name))

	body := alarmReq{
		Timestamp:    frame.Timestamp,
		Section:      frame.Section,
		Exent:        frame.Event,
		KnownPersons: persons,
		Image:        frame.Image,
		ExtraInfo:    frame.ExtraInfo,
	}

	marshalledBody, err := json.Marshal(body)
	if err != nil {
		return errors.New("failed to marshal body when sending the request: " + err.Error())
	}
	reqBody := bytes.NewBuffer(marshalledBody)

	resp, err := http.Post(alarmUrl, alarmContentType, reqBody)
	if err != nil {
		return errors.New("failed to post face recognition request: " + err.Error())
	}
	resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = errors.New("failed to post face recognition request: " + resp.Status)
	}

	a.Logger.Debug("alarm created", zap.String("code", resp.Status))

	return err
}
