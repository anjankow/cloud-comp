package app

import (
	"fmt"
	"sync"

	"go.uber.org/zap"
)

const (
	workerCount int = 5
	poolSize    int = 10
)

type App struct {
	Logger  *zap.Logger
	faceRec faceRecognition
	alarm   alarm

	queue chan Frame
	stop  chan bool
	wg    sync.WaitGroup
}

func NewApp(l *zap.Logger) App {

	return App{
		Logger:  l,
		faceRec: faceRecognition{Logger: l},
		alarm:   alarm{Logger: l},

		queue: make(chan Frame, poolSize),
		stop:  make(chan bool, workerCount),
	}
}

func (a *App) StartProcessing() {
	a.wg.Add(workerCount)
	for i := 0; i < workerCount; i++ {
		go func(i int) {
			defer a.wg.Done()
			a.worker(i)
		}(i)
	}
}

func (a *App) StopProcessing() {
	for i := 0; i < workerCount; i++ {
		a.stop <- true
	}
	a.wg.Wait()
	a.Logger.Debug("workers finished")
}

func (a *App) worker(id int) {
	a.Logger.Debug(fmt.Sprint("starting worker ", id))

	keepGoing := true
	for keepGoing {
		select {
		case <-a.stop:
			keepGoing = false
		case frame := <-a.queue:
			a.Logger.Debug(fmt.Sprint("worker ", id, " got a frame"))

			if err := a.processFrame(frame); err != nil {
				a.Logger.Error("processing the frame failed: "+err.Error(), zap.String("timestamp", frame.Timestamp))
			}
		}
	}
	a.Logger.Debug(fmt.Sprint("worker ", id, " terminated"))
}

func (a App) processFrame(frame Frame) error {
	// first recognize faces
	persons, err := a.faceRec.Recognize(frame)
	if err != nil {
		return err
	}

	if len(persons) == 0 {
		return nil
	}

	// send to the alarm service
	return a.alarm.Create(persons, frame)
}

func (a App) HandleFrame(frame Frame) {
	a.queue <- frame
}
