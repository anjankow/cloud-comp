package app

const (
	EventExit  = "exit"
	EventEnter = "entry"
)

type Frame struct {
	Timestamp      string
	Image          string
	Section        int
	Event          string
	DestinationUrl string `json:"destination"`
	ExtraInfo      string `json:"extra-info"`
}
