#!/bin/bash
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd ${SCRIPTDIR}/..

for SERVICE in $(cat ci/services.txt); do
    echo "Building "${SERVICE}
    docker build -f docker/Dockerfile --build-arg SERVICE_NAME=${SERVICE} -t registry.gitlab.com/anjankow/cloud-comp/${SERVICE} .
done
