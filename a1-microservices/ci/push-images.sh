#!/bin/bash
SCRIPTDIR="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd ${SCRIPTDIR}

for SERVICE in $(cat services.txt); do
    docker push registry.gitlab.com/anjankow/cloud-comp/${SERVICE}
done
