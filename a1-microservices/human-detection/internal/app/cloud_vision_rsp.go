package app

import "encoding/json"

type cloudVisionRspList struct {
	Responses []cloudVisionRspBody `json:"responses"`
}

type cloudVisionRspBody struct {
	ObjectAnnotations []annotation `json:"localizedObjectAnnotations"`
}

type annotation struct {
	Name string `json:"name"` // only this field is interesting for us
}

func unmarshalAnnotations(rspBody []byte) ([]annotation, error) {
	var unmarshalled cloudVisionRspList
	if err := json.Unmarshal(rspBody, &unmarshalled); err != nil {
		return []annotation{}, err
	}

	annotations := []annotation{}
	for _, rsp := range unmarshalled.Responses {
		annotations = append(annotations, rsp.ObjectAnnotations...)
	}

	return annotations, nil
}
