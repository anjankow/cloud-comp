package app

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

const (
	cloudVisionUrl         string = "https://vision.googleapis.com/v1/images:annotate"
	cloudVisionContentType string = "application/json"
	personName             string = "Person"
)

type App struct {
	Logger *zap.Logger
	apiKey string
}

func NewApp(l *zap.Logger, apiKey string) App {
	return App{
		Logger: l,
		apiKey: apiKey,
	}
}

func (a App) getCloudVisionUrl() string {
	return cloudVisionUrl + "?key=" + a.apiKey
}

func (a App) countPeople(annotations []annotation) int {
	cnt := 0
	for _, a := range annotations {
		if a.Name == personName {
			cnt++
		}
	}

	return cnt
}

func (a App) HandleFrame(frame Frame) (int, error) {
	a.Logger.Debug("posting to cloud vision", zap.String("url", a.getCloudVisionUrl()))

	// send to Cloud Vision
	marshalled, err := newCloudVisionReqBody(frame)
	if err != nil {
		return 0, errors.New("failed to create cloud vison request: " + err.Error())
	}
	reqBody := bytes.NewBuffer(marshalled)

	resp, err := http.Post(a.getCloudVisionUrl(), cloudVisionContentType, reqBody)
	if err != nil {
		return 0, errors.New("failed to post cloud vison request: " + err.Error())
	}
	defer resp.Body.Close()

	a.Logger.Debug("response received", zap.Any("code", resp.StatusCode), zap.Error(err), zap.Int("len", int(resp.ContentLength)))

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, errors.New("failed to read cloud vison response: " + err.Error())
	}

	annotations, err := unmarshalAnnotations(respBody)
	if err != nil {
		return 0, errors.New("failed to unmarshal annotations: " + err.Error())
	}

	return a.countPeople(annotations), nil
}
