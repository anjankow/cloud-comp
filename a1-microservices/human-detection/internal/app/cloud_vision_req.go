package app

import "encoding/json"

const (
	maxResults   int    = 10
	annotateType string = "OBJECT_LOCALIZATION"
)

type cloudVisionReqList struct {
	Requests []cloudVisionReqBody `json:"requests"`
}

type cloudVisionReqBody struct {
	Image    image     `json:"image"`
	Features []feature `json:"features"`
}

type image struct {
	Content string `json:"content"`
}

type feature struct {
	MaxResults int    `json:"maxResults"`
	Type       string `json:"type"`
}

func newCloudVisionReqBody(frame Frame) ([]byte, error) {
	cvReq := cloudVisionReqList{
		Requests: []cloudVisionReqBody{
			{
				Image: image{
					Content: frame.Image,
				},
				Features: []feature{
					{
						Type:       annotateType,
						MaxResults: maxResults,
					},
				},
			},
		},
	}

	return json.Marshal(cvReq)
}
