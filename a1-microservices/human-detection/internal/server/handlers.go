package server

import (
	"encoding/json"
	"errors"
	"human-detection/internal/app"
	"io/ioutil"
	"net/http"
)

type humanDetectionRsp struct {
	Timestamp   string `json:"timestamp"`
	Image       string `json:"image"`
	Section     int    `json:"section"`
	Event       string `json:"event"`
	PersonCount int    `json:"person-count"`
	ExtraInfo   string `json:"extra-info"`
}

// adds a new frame to the collector service
func frame(a *app.App, w http.ResponseWriter, r *http.Request) (int, error) {
	method := "POST"
	if r.Method != method {
		return http.StatusMethodNotAllowed, errors.New("incorrect method type: expected: " + method + ", received: " + r.Method)
	}

	contentType := "application/json"
	if r.Header.Get("Content-Type") != contentType {
		return http.StatusUnsupportedMediaType, errors.New("incorrect content-type: expected: " + contentType + ", received: " + r.Header.Get("Content-Type"))
	}

	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return http.StatusInternalServerError, errors.New("can't read the request body: " + err.Error())
	}

	var frame app.Frame
	if err = json.Unmarshal(body, &frame); err != nil {
		return http.StatusBadRequest, errors.New("invalid body: " + err.Error())
	}

	personCount, err := a.HandleFrame(frame)
	if err != nil {
		return http.StatusInternalServerError, errors.New("handler error: " + err.Error())
	}

	w.Header().Add("Content-Type", contentType)
	rsp := humanDetectionRsp{
		Timestamp:   frame.Timestamp,
		Image:       frame.Image,
		Section:     frame.Section,
		Event:       frame.Event,
		PersonCount: personCount,
		ExtraInfo:   frame.ExtraInfo,
	}

	marshalledRsp, err := json.Marshal(rsp)
	if err != nil {
		return http.StatusInternalServerError, errors.New("failed to marshal response: " + err.Error())
	}

	if num, err := w.Write(marshalledRsp); err != nil || num != len(marshalledRsp) {
		return http.StatusInternalServerError, errors.New("failed to write the response: " + err.Error())
	}

	return http.StatusOK, nil
}

func healthcheck(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("all good here"))
}
