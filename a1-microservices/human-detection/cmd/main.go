package main

import (
	"human-detection/internal/app"
	"human-detection/internal/server"
	"log"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	hardcodedApiKey = "AIzaSyClkrL3Zl5mshraJ8_oJKyYeTNJCXK0RrA"
	defaultAddr     = ""
)

func main() {

	logger, err := getLogger()
	if err != nil {
		log.Fatalln("Setting up the logger failed: ", err)
		return
	}
	defer logger.Sync()

	logger.Info("Service started")

	apiKey := os.Getenv("API_KEY")
	if apiKey == "" {
		apiKey = hardcodedApiKey
	}
	logger.Info("api key: " + apiKey)

	service := app.NewApp(logger, apiKey)
	if err != nil {
		logger.Fatal("New service creation failed: ", zap.Error(err))
		return
	}

	// HTTP SERVER
	port := os.Getenv("PORT")
	var addr string
	if port != "" {
		addr = ":" + port
	}

	ser := server.NewServer(logger, &service, addr)
	if err != nil {
		logger.Fatal("Server creation failed: ", zap.Error(err))
	}

	err = ser.Run()

	if err != nil {
		logger.Error("Service finished with error", zap.Error(err))
	} else {
		logger.Info("Service finished")
	}
}

func getLogger() (*zap.Logger, error) {
	options := []zap.Option{
		zap.AddCaller(),
		zap.AddStacktrace(zap.FatalLevel),
	}

	config := zap.NewDevelopmentConfig()
	config.EncoderConfig.EncodeTime = zapcore.TimeEncoderOfLayout(time.RFC3339)
	config.Development = true
	config.Level.SetLevel(zap.DebugLevel)

	logger, err := config.Build()
	return logger.WithOptions(options...), err
}
