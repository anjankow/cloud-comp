# Airport monitoring system

## Layout
Each service implementation is in a separate directory.
They use common Dockerfile placed inside the docker/ folder.

## Starting up the project

Make sure that the API_KEY is set in the human-detection/.env file and run

```sh
docker-compose up
```
