package server

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"

	"go.uber.org/zap"
)

func forwardRequest(logger *zap.Logger, getDst getDstUrl, w http.ResponseWriter, r *http.Request) (int, error) {
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return http.StatusInternalServerError, errors.New("can't read the request body: " + err.Error())
	}

	newUrl := getDst(r.URL)
	logger.Info("forwarding to " + newUrl)
	newBody := bytes.NewBuffer(body)

	newReq, err := http.NewRequest(r.Method, newUrl, newBody)
	if err != nil {
		return http.StatusInternalServerError, errors.New("can't create a new reuqest: " + err.Error())
	}
	defer newReq.Body.Close()
	newReq.Header = r.Header.Clone()

	resp, err := http.DefaultClient.Do(newReq)
	if err != nil {
		return http.StatusInternalServerError, errors.New("new reuqest error: " + err.Error())
	}

	logger.Debug("received a response", zap.String("status_code", resp.Status), zap.Int("len", int(resp.ContentLength)))

	w.WriteHeader(resp.StatusCode)

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return http.StatusInternalServerError, errors.New("reading the response body failed: " + err.Error())
	}

	if n, err := w.Write(respBody); err != nil || n != len(respBody) {
		return http.StatusInternalServerError, errors.New("writing the response body failed: " + err.Error())
	}

	return http.StatusOK, nil
}

func healthcheck(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("all good here"))
}
