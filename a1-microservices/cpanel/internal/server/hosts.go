package server

import (
	"net/url"
)

const (
	alertHost     = "alert"
	collectorHost = "collector"
	sectionHost   = "section"
)

func getDstUrlAlert(srcUrl *url.URL) string {
	return "http://" + alertHost + srcUrl.RequestURI()
}

func getDstUrlCollector(srcUrl *url.URL) string {
	// url := strings.ReplaceAll(srcUrl.RequestURI(), collectorHost, "")
	return "http:/" + srcUrl.RequestURI()
}

func getDstUrlSection(srcUrl *url.URL) string {
	return "http:/" + srcUrl.RequestURI()
}
