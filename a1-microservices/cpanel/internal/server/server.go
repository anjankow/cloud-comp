package server

import (
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

type server struct {
	logger     *zap.Logger
	httpServer *http.Server
}

type appHandler struct {
	logger  *zap.Logger
	dstHost getDstUrl
}

type getDstUrl func(srcUrl *url.URL) string

func (ser server) registerHandlers(router *mux.Router) {
	router.PathPrefix("/alerts").Handler(appHandler{logger: ser.logger, dstHost: getDstUrlAlert})
	router.PathPrefix("/section").Handler(appHandler{logger: ser.logger, dstHost: getDstUrlSection})
	router.PathPrefix("/collector").Handler(appHandler{logger: ser.logger, dstHost: getDstUrlCollector})

	router.HandleFunc("/", healthcheck)
}

func NewServer(logger *zap.Logger) server {
	return server{
		logger: logger,
	}
}

func (ser server) Run() error {
	router := mux.NewRouter()
	ser.registerHandlers(router)

	ser.httpServer = &http.Server{
		Handler:  router,
		ErrorLog: zap.NewStdLog(ser.logger),
	}

	return ser.httpServer.ListenAndServe()
}

func (appHndl appHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	appHndl.logger.Debug("request received", zap.String("method", r.Method), zap.String("url", r.URL.Path), zap.String("content-type", r.Header.Get("Content-Type")))
	status, err := forwardRequest(appHndl.logger, appHndl.dstHost, w, r)

	if err != nil {
		appHndl.logger.Warn("request failed", zap.Error(err))
		w.Write([]byte(err.Error()))

		switch status {
		case http.StatusNotFound:
			http.NotFound(w, r)
		case http.StatusInternalServerError:
			http.Error(w, http.StatusText(status), status)
		default:
			http.Error(w, http.StatusText(status), status)
		}
		return
	}
}
